﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace vovo4ka
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            text.Text = "Вовочка учится в 4 классе и каждую среду у него самые простые уроки:\n физкультура, музыка и технология." +
                " И каждую среду\n Вовочка находится в раздумьях идти ему в школу или нет,\n но так как его маме на работу только к 9 часам," +
                " то\n каждый раз ему приходится заставлять себя просыпаться в 7 часов\n и идти на уроки к 8," +
                " ведь ему очень не хотелось огорчать маму.";
        }
        character vova = new character(false,false);

        private void button1_Click(object sender, EventArgs e)
        {
            text.Text = "Открыв глаза Вовочка сразу же отправляется...";
            start.Visible = false;
            vihod.Visible = true;kuhnya.Visible = true;
        }

        private void kuhnya_Click(object sender, EventArgs e)
        {
            text.Text = "мама перед уходом оставила бутерброд.";
            vihod.Visible = false;kuhnya.Visible = false;
            buter.Visible = true;
        }

        private void vihod_Click(object sender, EventArgs e)
        {
            text.Text = "В 7:30 Вовочка вышел из дома, шёл очень медленно и постоянно думал:\n" +
                " «Зачем вообще нужны такие бесполезные предметы в школе». Школа\n находилась" +
                " примерно в 15 минутах ходьбы от дома, но так как Вовочке\n очень не хотелось туда идти," +
                " то он шёл очень-очень медленно.\n Увидел он площадку, на которой гуляли другие ребята и" +
                " минут 5 стоял,\n размышлял идти ему гулять или всё-таки пойти в школу.\nВ какой-то момент Вова вспомнил, что забыл закрыть квартиру...";
            vihod.Visible = false; kuhnya.Visible = false;
            ploshadka.Visible = true; vernutsya.Visible = true; baaabka.Visible = true;
        }

        private void buter_Click(object sender, EventArgs e)
        {
            text.Text = "Вместе с бутербродом Вовочка тут же отправляется в путь.\nВ 7:30 Вовочка вышел из дома, шёл очень медленно и постоянно думал:\n" +
                " «Зачем вообще нужны такие бесполезные предметы в школе». Школа\n находилась" +
                " примерно в 15 минутах ходьбы от дома, но так как Вовочке\n очень не хотелось туда идти," +
                " то он шёл очень-очень медленно.\n Увидел он площадку, на которой гуляли другие ребята и" +
                " минут 5 стоял,\n размышлял идти ему гулять или всё-таки пойти в школу.\n В какой-то момент Вова вспомнил, что забыл закрыть квартиру...\n";
            buter.Visible = false;
            vova.Buterbrod = true;
            end.Visible = true;
            ploshadka.Visible = true; vernutsya.Visible = true; baaabka.Visible = true;
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            text.Text = "бутерброд оказался кусочком сущности\n древних богов и поглотил Вовочку целиком.";
            wakeupjesse.Visible = true;
            end.Visible = false;
            ploshadka.Visible = false; vernutsya.Visible = false; baaabka.Visible = false; gobabushka.Visible = false; larek.Visible = false;
        }

        private void wakeupjesse_Click(object sender, EventArgs e)
        {
            text.Text = "Открыв глаза Вовочка сразу же отправляется...";
            start.Visible = false; wakeupjesse.Visible = false;
            vihod.Visible = true; kuhnya.Visible = true;
        }

        private void ploshadka_Click(object sender, EventArgs e)
        {
            text.Text = "Вова, снова вспомнив про маму и про то как она расстроится,\n" +
                " пошёл расстроенный в школу. Если бы он всё-таки пошёл\n" +
                " на площадку, то маме бы позвонила классная руководительница\n" +
                " и Вову бы наказали, оставив без компьютера и гулянок,\n" +
                " постоянно сидя за уроками, чтобы больше не прогуливал.";
        }

        private void vernutsya_Click(object sender, EventArgs e)
        {
            vova.Kluchh = true;
            text.Text = "Вова очень быстро добежал обратно и закрыл квартиру";
        }

        private void baaabka_Click(object sender, EventArgs e)
        {
            text.Text = "Тут он подумал, что можно пойти к бабушке и у неё посидеть,\n отдохнуть, вкусно покушать.\n Недалеко от бабушкиного дома красуется яркая вывеска\n будки с эскимо.";
            ploshadka.Visible = false; vernutsya.Visible = false; baaabka.Visible = false;
            gobabushka.Visible = true; larek.Visible = true;
        }

        private void gobabushka_Click(object sender, EventArgs e)
        {
            text.Text = "Вовремя одумавшись, Вова пошёл дальше. Пошёл бы Вовочка к бабушке,\n она бы отругала его и отвела за руку в школу, ещё бы и маме рассказала.\n";
                if (vova.Kluchh == true)
            {
                text.Text += "Вова пошёл в школу,где смог получить кучу хороших оценок\n и порадовать маму и бабушку, они похвалили его и купили ему\n плейстейшен, о котором он так давно мечтал.";
            }
            else { text.Text += "По возвращению из школы, Вовочка обнаружил,\n что из квартиры его семьи вынесли всё металлическое, даже ложки\n и вилки, а дедушка, оставшийся за старшего, лежит в луже крови на полу,\n всё могло быть иначе, не забудь Вова закрыть дверь на ключ..."; }
            gobabushka.Visible = false; larek.Visible = false;
        }

        private void larek_Click(object sender, EventArgs e)
        {
            text.Text = "Перед ларьком с мороженым Вова не устоял и решил\n" +
                    " зайти за эскимо. Подошел за мороженым и увидел\n" +
                    " своего друга Сашку, они учились в одном классе. Сашка\n" +
                    " уговорил Вову не идти в школу и тот согласился.\n" +
                    " Мальчики веселые побежали на площадку, качаться на качелях\n" +
                    " и кататься с горки. Недолго длилось их счастье, мама\n" +
                    " Вовочки пошла на работу и увидела ребят,\n" +
                    " подошла к ним и очень долго ругала их.\n" +
                    "Настолько долго, что уроки просто успели закончиться...";
            gobabushka.Visible = false; larek.Visible = false;
        }
    }
}
