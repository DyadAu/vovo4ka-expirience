﻿namespace vovo4ka
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.text = new System.Windows.Forms.Label();
            this.start = new System.Windows.Forms.Button();
            this.kuhnya = new System.Windows.Forms.Button();
            this.vihod = new System.Windows.Forms.Button();
            this.buter = new System.Windows.Forms.Button();
            this.end = new System.Windows.Forms.Button();
            this.wakeupjesse = new System.Windows.Forms.Button();
            this.ploshadka = new System.Windows.Forms.Button();
            this.vernutsya = new System.Windows.Forms.Button();
            this.baaabka = new System.Windows.Forms.Button();
            this.gobabushka = new System.Windows.Forms.Button();
            this.larek = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // text
            // 
            this.text.AutoSize = true;
            this.text.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.text.Location = new System.Drawing.Point(19, 15);
            this.text.Name = "text";
            this.text.Size = new System.Drawing.Size(0, 26);
            this.text.TabIndex = 0;
            // 
            // start
            // 
            this.start.Font = new System.Drawing.Font("Microsoft Sans Serif", 30.25F);
            this.start.Location = new System.Drawing.Point(213, 319);
            this.start.Name = "start";
            this.start.Size = new System.Drawing.Size(355, 74);
            this.start.TabIndex = 1;
            this.start.Text = "начать квест";
            this.start.UseVisualStyleBackColor = true;
            this.start.Click += new System.EventHandler(this.button1_Click);
            // 
            // kuhnya
            // 
            this.kuhnya.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.kuhnya.Location = new System.Drawing.Point(449, 319);
            this.kuhnya.Name = "kuhnya";
            this.kuhnya.Size = new System.Drawing.Size(212, 72);
            this.kuhnya.TabIndex = 2;
            this.kuhnya.Text = "на кухню";
            this.kuhnya.UseVisualStyleBackColor = true;
            this.kuhnya.Visible = false;
            this.kuhnya.Click += new System.EventHandler(this.kuhnya_Click);
            // 
            // vihod
            // 
            this.vihod.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.vihod.Location = new System.Drawing.Point(165, 319);
            this.vihod.Name = "vihod";
            this.vihod.Size = new System.Drawing.Size(212, 72);
            this.vihod.TabIndex = 3;
            this.vihod.Text = "в путь";
            this.vihod.UseVisualStyleBackColor = true;
            this.vihod.Visible = false;
            this.vihod.Click += new System.EventHandler(this.vihod_Click);
            // 
            // buter
            // 
            this.buter.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.buter.Location = new System.Drawing.Point(298, 319);
            this.buter.Name = "buter";
            this.buter.Size = new System.Drawing.Size(212, 72);
            this.buter.TabIndex = 4;
            this.buter.Text = "взять бутерброд";
            this.buter.UseVisualStyleBackColor = true;
            this.buter.Visible = false;
            this.buter.Click += new System.EventHandler(this.buter_Click);
            // 
            // end
            // 
            this.end.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.end.Location = new System.Drawing.Point(667, 319);
            this.end.Name = "end";
            this.end.Size = new System.Drawing.Size(131, 119);
            this.end.TabIndex = 5;
            this.end.Text = "cъесть бутерброд";
            this.end.UseVisualStyleBackColor = true;
            this.end.Visible = false;
            this.end.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // wakeupjesse
            // 
            this.wakeupjesse.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.wakeupjesse.Location = new System.Drawing.Point(298, 319);
            this.wakeupjesse.Name = "wakeupjesse";
            this.wakeupjesse.Size = new System.Drawing.Size(212, 72);
            this.wakeupjesse.TabIndex = 6;
            this.wakeupjesse.Text = "ПРОСНУТЬСЯ";
            this.wakeupjesse.UseVisualStyleBackColor = true;
            this.wakeupjesse.Visible = false;
            this.wakeupjesse.Click += new System.EventHandler(this.wakeupjesse_Click);
            // 
            // ploshadka
            // 
            this.ploshadka.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.ploshadka.Location = new System.Drawing.Point(12, 319);
            this.ploshadka.Name = "ploshadka";
            this.ploshadka.Size = new System.Drawing.Size(212, 72);
            this.ploshadka.TabIndex = 7;
            this.ploshadka.Text = "пойти играть";
            this.ploshadka.UseVisualStyleBackColor = true;
            this.ploshadka.Visible = false;
            this.ploshadka.Click += new System.EventHandler(this.ploshadka_Click);
            // 
            // vernutsya
            // 
            this.vernutsya.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.vernutsya.Location = new System.Drawing.Point(449, 319);
            this.vernutsya.Name = "vernutsya";
            this.vernutsya.Size = new System.Drawing.Size(212, 72);
            this.vernutsya.TabIndex = 8;
            this.vernutsya.Text = "вернуться";
            this.vernutsya.UseVisualStyleBackColor = true;
            this.vernutsya.Visible = false;
            this.vernutsya.Click += new System.EventHandler(this.vernutsya_Click);
            // 
            // baaabka
            // 
            this.baaabka.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.baaabka.Location = new System.Drawing.Point(231, 319);
            this.baaabka.Name = "baaabka";
            this.baaabka.Size = new System.Drawing.Size(212, 72);
            this.baaabka.TabIndex = 9;
            this.baaabka.Text = "идти дальше";
            this.baaabka.UseVisualStyleBackColor = true;
            this.baaabka.Visible = false;
            this.baaabka.Click += new System.EventHandler(this.baaabka_Click);
            // 
            // gobabushka
            // 
            this.gobabushka.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.gobabushka.Location = new System.Drawing.Point(165, 319);
            this.gobabushka.Name = "gobabushka";
            this.gobabushka.Size = new System.Drawing.Size(212, 72);
            this.gobabushka.TabIndex = 10;
            this.gobabushka.Text = "пойти к бабушке";
            this.gobabushka.UseVisualStyleBackColor = true;
            this.gobabushka.Visible = false;
            this.gobabushka.Click += new System.EventHandler(this.gobabushka_Click);
            // 
            // larek
            // 
            this.larek.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F);
            this.larek.Location = new System.Drawing.Point(397, 319);
            this.larek.Name = "larek";
            this.larek.Size = new System.Drawing.Size(212, 72);
            this.larek.TabIndex = 11;
            this.larek.Text = "дойти до ларька";
            this.larek.UseVisualStyleBackColor = true;
            this.larek.Visible = false;
            this.larek.Click += new System.EventHandler(this.larek_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.larek);
            this.Controls.Add(this.gobabushka);
            this.Controls.Add(this.baaabka);
            this.Controls.Add(this.vernutsya);
            this.Controls.Add(this.ploshadka);
            this.Controls.Add(this.wakeupjesse);
            this.Controls.Add(this.end);
            this.Controls.Add(this.buter);
            this.Controls.Add(this.vihod);
            this.Controls.Add(this.kuhnya);
            this.Controls.Add(this.start);
            this.Controls.Add(this.text);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label text;
        private System.Windows.Forms.Button start;
        private System.Windows.Forms.Button kuhnya;
        private System.Windows.Forms.Button vihod;
        private System.Windows.Forms.Button buter;
        private System.Windows.Forms.Button end;
        private System.Windows.Forms.Button wakeupjesse;
        private System.Windows.Forms.Button ploshadka;
        private System.Windows.Forms.Button vernutsya;
        private System.Windows.Forms.Button baaabka;
        private System.Windows.Forms.Button gobabushka;
        private System.Windows.Forms.Button larek;
    }
}

